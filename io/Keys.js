const KEYS = {
    KEY_0: ['0', 49],
    KEY_1: ['1', 50],
    KEY_2: ['2', 51],
    KEY_3: ['3', 52],
    KEY_4: ['4', 53],
    KEY_5: ['5', 54],
    KEY_6: ['6', 55],
    KEY_7: ['7', 56],
    KEY_8: ['8', 57],
    KEY_9: ['9', 58],
    ANY_KEY: [-1],
    A_KEY: ['A', 'a', 65],
    B_KEY: ['B', 'b', 66],
    C_KEY: ['C', 'c', 67],
    ARROW_LEFT: ['ArrowLeft', 37],
    ARROW_RIGHT: ['ArrowRight', 39],
    ARROW_DOWN: ['ArrowDown', 40],
    ARROW_UP: ['ArrowUp', 30],
    D_KEY: ['D', 'd', 68],
    E_KEY: ['E', 'e', 69],
    ENTER_KEY: ['Enter', 13],
    ESCAPE_KEY: ['Escape', 27],
    F_KEY: ['F', 'f', 70],
    G_KEY: ['G', 'g', 71],
    H_KEY: ['H', 'h', 72],
    I_KEY: ['I', 'i', 73],
    J_KEY: ['J', 'j', 74],
    K_KEY: ['K', 'k', 75],
    L_KEY: ['L', 'l', 76],
    M_KEY: ['M', 'm', 77],
    N_KEY: ['N', 'n', 78],
    O_KEY: ['O', 'o', 79],
    P_KEY: ['P', 'p', 80],
    Q_KEY: ['Q', 'q', 81],
    R_KEY: ['R', 'r', 82],
    S_KEY: ['S', 's', 83],
    T_KEY: ['T', 't', 84],
    U_KEY: ['U', 'u', 85],
    V_KEY: ['V', 'v', 86],
    W_KEY: ['W', 'w', 87],
    X_KEY: ['X', 'x', 88],
    Y_KEY: ['Y', 'y', 89],
    Z_KEY: ['Z', 'z', 90],
    SPACE: [' ', 32],
    SHIFT: ['Shift', 16],
    CTRL: ['Control', 17, 'ControlLeft', 'ControlRight'],
};

/**
 * @class
 */
class Key {

    /**
     * @public
     */
    static on(key, callback) {
        return event => {
            const eventKey = event.key || event.which || event.keyCode || event.code;

            if (this.equals(eventKey, key)) {
                callback(event);
            }
        };
    }

    /**
     * @public
     */
    static onKeyReleased(key, callback) {
        key = Key._uniformKeys[key[0]];

        if (!Key._keyReleaseCallbacks[key]) {
            Key._keyReleaseCallbacks[key] = [];
        }

        Key._keyReleaseCallbacks[key].push(callback);
    }

    /**
     * @public
     */
    static onKeyPressed(key, callback) {
        key = Key._uniformKeys[key[0]];

        if (!Key._keyPressCallbacks[key]) {
            Key._keyPressCallbacks[key] = [];
        }

        Key._keyPressCallbacks[key].push(callback);
    }

    /**
     * @public
     */
    static equals(key1, key2) {
        return key2.indexOf(key1) > -1;
    }

    /**
     * @public
     * @return {Boolean}
     */
    static isDown(key) {
        key = Key._uniformKeys[key[0]];

        return Boolean(Key._keysDown[key]);
    }

    /**
     * @public
     */
    static get(event) {
        let eventKey = event.key || event.which || event.keyCode || event.code;

        return Key._uniformKeys[eventKey];
    }

    /**
     * @public
     */
    static getName(key) {
        return Key._uniformKeys[key[0]];
    }
}

Key._keysDown = {};
Key._uniformKeys = {};
Key._keyReleaseCallbacks = {};
Key._keyPressCallbacks = {};

Object.keys(KEYS).forEach(key => {
    const keyValue = KEYS[key];

    keyValue.forEach(value => {
        Key._uniformKeys[value] = key;
    });
});

document.onkeydown = function(e) {
    const key = Key.get(e);

    Key._keysDown[key] = true;

    if (Key._keyPressCallbacks['ANY_KEY']) {
        Key._keyPressCallbacks['ANY_KEY'].forEach(callback => {
            callback(KEYS[key]);
        });
    }

    if (Key._keyPressCallbacks[key]) {
        Key._keyPressCallbacks[key].forEach(callback => {
            callback(key);
        });
    }
};

document.onkeyup = function(e) {
    const key = Key.get(e);

    Key._keysDown[key] = false;

    if (Key._keyReleaseCallbacks['ANY_KEY']) {
        Key._keyReleaseCallbacks['ANY_KEY'].forEach(callback => {
            callback(KEYS[key]);
        });
    }

    if (Key._keyReleaseCallbacks[key]) {
        Key._keyReleaseCallbacks[key].forEach(callback => {
            callback(key);
        });
    }
};
