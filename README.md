# JSUtils

[Read the documentation](https://jdsutton.gitlab.io/JSUtils/)

[Learn JSUtils: Take the Course](https://www.udemy.com/course/draft/3341416/?referralCode=BF1C119DFF566B7938DE)

JSUtils provides a robust and painless framework for frontend development. 

JSUtils offers modular libraries which can be imported individually, keeping your dependencies in check and avoiding bloat.

Specifically, JSUtils offers:

- Component-based UI programming
- Automated component & page generation
- Automatic rebuilds triggered on source code changes
- Common JS utilities
- Generic RPC functionality

## Installation

**Clone the repo:**

```bash
git clone https://gitlab.com/jdsutton/JSUtils.git
```
```bash
git@gitlab.com:jdsutton/JSUtils.git
```

>NOTE: Install **Python3.6** if not already installed. 

 **Create a [virtualenv](https://pypi.org/project/virtualenv/) and install.**

```bash
make create_env

. env/bin/activate

make install
```

## Testing

Tests are driven by `Mocha` and a custom assertion library. 

```bash
make test
```

## Building the docs

```bash
make docs
```

Viewing the docs: 

```bash
cd documentation && python3 -m http.server
```

Then navigate to `localhost:8000` to view the docs. 

## Generate a component

```bash
./src/JSUtils/scripts/create_component.sh dir/ACoolComponent
```

Output will be under `./src/components/dir/ACoolComponent`

## Generate a view

```bash
./src/JSUtils/scripts/create_view.sh dir/myview MyCoolView
```
