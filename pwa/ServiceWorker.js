/**
 * @class
 * https://developers.google.com/web/fundamentals/primers/service-workers/
 * https://developers.google.com/web/ilt/pwa/lab-caching-files-with-service-worker
 */
class ServiceWorker {
        
        /**
         * @public
         */
        static cache(filesToCache, cacheFirst) {
            self.addEventListener('install', event => {
                console.debug('Attempting to install service worker and cache static assets');
                event.waitUntil(
                    caches.open(ServiceWorker._cacheName).then(cache => {
                            return cache.addAll(filesToCache);
                    })
                );
            });

            self.addEventListener('activate', event => {
                console.debug('Activating new service worker...');

                event.waitUntil(this._tryClearCache());
            });

            self.addEventListener('fetch', event => {
                console.debug('Fetch event for ', event.request.url);

                let response;

                if (cacheFirst) {
                    response = ServiceWorker._getFileFromCache(event);
                } else {
                    response = ServiceWorker._getFileFromServer(event).catch(err => {
                        return ServiceWorker._getFileFromCache(event);
                    });
                }

                event.respondWith(response);
            });
        }

        /**
         * @private
         */
        static _getFileFromCache(event) {
            return caches.match(event.request).then(response => {
                if (response) {
                        console.debug('Found ', event.request.url, ' in cache');
                        return response;
                }

                return ServiceWorker._getFileFromServer(event);
            }).catch(error => {
                // TODO 6 - Respond with custom offline page
            });
        }

        /**
         * @private
         */
        static _getFileFromServer(event) {
            console.debug('Network request for ', event.request.url);

            return fetch(event.request).then(response => {
                // TODO 5 - Respond with custom 404 page

                // Store response in cache.
                return caches.open(ServiceWorker._cacheName).then(cache => {
                        cache.put(event.request.url, response.clone());
                        
                        return response;
                });
            });
        }

        /**
         * @public
         */
        static register(path) {
            console.debug('Attempting to register service worker at: ' + path);

            if ('serviceWorker' in navigator) {
                window.addEventListener('load', function() {
                    navigator.serviceWorker.register(path).then(function(registration) {
                        ServiceWorker._registration = registration;

                        console.debug(
                            'ServiceWorker registration successful with scope: ',
                            registration.scope,
                        );

                        ServiceWorker._tryClearCache();
                        
                    }, function(err) {
                            console.debug('ServiceWorker registration failed: ', err);
                    });
                });
            } else {
                    console.warn('Service worker not supported.');
            }
        }

        /**
         * @public
         */
        static _tryClearCache() {
            const cacheWhitelist = [ServiceWorker._cacheName];

            return caches.keys().then(cacheNames => {
                return Promise.all(
                    cacheNames.map(cacheName => {
                        if (cacheWhitelist.indexOf(cacheName) === -1) {
                            return caches.delete(cacheName);
                        }
                    })
                ).then(() => {
                        console.debug('Cleared service worker cache.');
                });
            })
        }

        /**
         * @public
         */
        static getRegistration() {
            return ServiceWorker._registration;
        }
}

ServiceWorker._registration = null;
ServiceWorker._cacheName = 'cache[["versionIdentifier"]]';
