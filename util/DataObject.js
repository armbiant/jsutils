/**
 * An object carrying data to/from the backend.
 * @class
 */
class DataObject {

    /**
     * @public
     * @param {Object} d
     */
    constructor(d, propertyCheck, transformKeys) {
        if (d) {
            Object.assign(this, this.constructor.fromDict(d, propertyCheck, transformKeys));
        }

        this.init();
    }

    copy() {
        return new this.constructor(this);
    }

    /**
     * @private
     */
    static _transformKey(key) {
        let newKey = key.trim().split(' ');

        newKey[0] = newKey[0].substr(0, 1).toLowerCase()
            + newKey[0].substr(1);

        for (let i = 1; i < newKey.length; i++) {
            newKey[i] = newKey[i].substr(0, 1).toUpperCase()
                + newKey[i].substr(1);
        }

        return newKey.join('');
    }

    /**
     * @public
     * Builds an instance of this class using properties from an object.
     * @param {Object} d
     * @param {Boolean} propertyCheck - Whether or not to only use values for this class' instance properties.
     * @param {Boolean} transformKeys
     * @return {DataObject}
     */
    static fromDict(d, propertyCheck, transformKeys) {
        let result = new this();
        if (propertyCheck === undefined) propertyCheck = true;

        for (let key in d) {
            let newKey = key;

            if (transformKeys) {
                newKey = this._transformKey(newKey);
            }

            if (propertyCheck && !(newKey in result)) {
                console.debug('Warning: Attempt to set unkown property on ' + this.name + ' instance: ' + newKey);
                console.debug((new Error()).stack);
            } else {
                result[newKey] = d[key];
            }
        }

        result.init();

        return result;
    }

    /**
     * @public
     * @abstract
     */
    init() {

    }
}
