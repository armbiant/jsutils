/**
 * @class
 */
class File {

    /**
     * @public
     */
    static createDataURL(data, type) {
        let file = new Blob([data], {type:  type || 'text/plain'});
        const b64 = btoa(data);

        return `data:${type};base64,${b64}`;
    }

    /**
     * @public
     * https://stackoverflow.com/questions/13405129/javascript-create-and-save-file
     */
    static save(data, filename, type) {
        let file = new Blob([data], {type: type});
        const url = URL.createObjectURL(file);

        if (window.navigator.msSaveOrOpenBlob) // IE10+
            window.navigator.msSaveOrOpenBlob(file, filename);
        else { // Others
            let a = document.createElement("a");
            a.href = url;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            setTimeout(function() {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);  
            }, 0); 
        }
    }

    /**
     * @public
     */
    static upload(fileInput, endpoint) {
        let formData = new FormData();

        formData.append('file', fileInput.files[0]);

        return Request.post(endpoint, formData, null, true).then(response => {
            return JSON.parse(response).fileName;
        });
    }

    /**
     * @public
     */
    static readAsText(fileInput) {
        const file = fileInput.files[0];
        const reader = new FileReader();
        
        return new Promise((resolve, reject) => {
            reader.onload = e => {
                resolve(reader.result);
            };

            reader.readAsText(file);
        });
    }


    /**
     * @public
     */
    static readAsDataURL(fileInput) {
        const file = fileInput.files[0];
        const reader = new FileReader();
        
        return new Promise((resolve, reject) => {
            reader.onload = e => {
                resolve(reader.result);
            };

            reader.onabort = reject;
            reader.onerror = reject;

            reader.readAsDataURL(file);
        });
    }

    /**
     * @public
     */
    static select(mode, extensions) {
        let input = document.createElement('input');
        input.type = 'file';
        mode = mode || 'data';

        if (extensions) {
            input.accept = extensions.join(',');
        }
 
        input.click();

        return new Promise((resolve, reject) => {
            input.onchange = e => {
                let result;

                if (mode === 'data') result = this.readAsDataURL(e.target);
                else result = this.readAsText(e.target) ;
                
                resolve(result);
            };

            input.onblur = reject;
        });
    }
}