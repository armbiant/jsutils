/**
 * @class
 */
class HTMLString {
    
    /**
     * @public
     */
    static escape(s) {
        if (!s) return s;
        
        return s.replace(this._PATTERN, (match) => {
            return this._REPLACEMENTS[match];
        });
    }

    /**
     * @public
     */
    static unescape(s) {
        if (!s) return s;

        Object.keys(this._REPLACEMENTS).forEach(key => {
            const entity = this._REPLACEMENTS[key];

            s = s.replaceAll(entity, key);
        });

        return s;
    }
}

HTMLString._PATTERN = /[&<>"'\/]/g;
HTMLString._REPLACEMENTS = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#x27;',
    '/': '&#x2F;',
};