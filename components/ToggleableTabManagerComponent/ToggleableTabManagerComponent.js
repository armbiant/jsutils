import('../Component.js');
import('../TabManager.js');
import('../Toggleable.js');

/**
 * @class
 * @param {String[]} labels
 * @param {String[]} containerIds
 */
class ToggleableTabManagerComponent extends Component {

    /**
     * @constructor
     */
    constructor(labels, containerIds) {
        let params = {};

        super(ToggleableTabManagerComponent.ID.TEMPLATE.THIS, params);

        this._tabManager = new TabManager();

        let defaultTabCreated = false;

        for (let i=0; i < containerIds.length; i++) {
            if (!defaultTabCreated) {
                this._tabManager.addTab(containerIds[i], 'flex', 'flex');
                defaultTabCreated = true;
            } else {
                this._tabManager.addTab(containerIds[i], 'none', 'flex');
            }
        }

        let toggleables = [];

        for (let i=0; i < labels.length; i++) {
            const states = {
                'unselected': labels[i],
                'selected' : labels[i],
            }

            const onToggle = state => {
                if (state === 'selected') {
                    this._tabManager.showTab(containerIds[i]);
                }
            }

            const toggleable = new Toggleable(states, null, this.constructor.CLASS.LINK_SELECTOR)
                .setOnToggle(onToggle);

            toggleables.push(toggleable);
        }

        if (toggleables.length)
            toggleables[0].setState('selected');

        Toggleable.createRadioGroup(toggleables);

        this._toggleables = toggleables;

        this.setOnRender(() => {
            this._tabManager.showTab(containerIds[0]);
        });
    }

    /**
     * @public
     */
    getSelected() {
        let result = null;

        this._toggleables.forEach(toggleable => {
            if (toggleable.getState() === 'selected') {
                result = toggleable;
            }
        });

        return result;
    }

    /**
     * @public
     * @override
     */
    render() {
        return this._toggleables.reduce((accumulator, toggleable) => {
            return accumulator + Dom.elementToHTML(toggleable.render());
        }, '');
    }

    /**
     * @public
     * @param {String} id
     * @param {Function} f
     */
    setOnShowTab(id, f) {
        this._tabManager.setOnShowTab(id, f);

        return this;
    }

    /**
     * @public
     * @param {String} id
     * @param {Function} f
     */
    setOnHideTab(id, f) {
        this._tabManager.setOnHideTab(id, f);

        return this;
    }

    /**
     * Makes a tab visible in the UI.
     * @param {string} id - The id of the element to show.
     */
    showTab(id) {
        this._tabManager.showTab(id);

        return this;
    }
}

ToggleableTabManagerComponent.CLASS = {
    LINK_SELECTOR: 'LinkSelector',
};

ToggleableTabManagerComponent.ID = {
    TEMPLATE: {
        THIS: 'ToggleableTabManagerComponent_template',
    },
};
