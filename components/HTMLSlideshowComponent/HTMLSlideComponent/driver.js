import('/src/JSUtils/components/Component.js');

/**
 * @class
 */
class HTMLSlideComponent extends Component {

    /**
     * @constructor
     */
    constructor(html) {
        const params = {html};
        
        super(HTMLSlideComponent.ID.TEMPLATE.THIS, params);
    }
}

HTMLSlideComponent.ID = {
    TEMPLATE: {
        THIS: 'HTMLSlideComponent_template',
    },
};
