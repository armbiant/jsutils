/**
 * @class
 */
class ConfirmationModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor(onConfirm, confirmString) {
        const params = {
            query: confirmString || 'Are you sure you want to do that?',
        };

        super(ConfirmationModalComponent.ID.TEMPLATE.THIS, params);

        this._onConfirm = onConfirm;
    }

    /**
     * @public
     */
    setQuery(query) {
        this._params.query = query;

        return this;
    }

    /**
     * @public
     */
    onConfirmButtonClicked() {
        this._onConfirm();
        this.close();
    }
}

ConfirmationModalComponent.ID = {
    TEMPLATE: {
        THIS: 'ConfirmationModalComponent_template',
    },
};
