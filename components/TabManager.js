// nodist
import('../util/Dom.js');

/**
 * A class for managing tabs.
 * @class
 * @public
 */
class TabManager {
    constructor() {
        this._ids = new Set();
        this._initialDisplayById = {};
        this._visibleDisplayById = {};
        this._onShowTabById = {};
        this._onHideTabById = {};
    }

    /**
     * Register a tab.
     * @param {string} id - The id of the element to use as a tab.
     * @param {string} initialDisplay - The initial display value for the tab.
     * @param {string} visibleDisplay - The visible display value for the tab.
     * @return {TabManager} this
     */
    addTab(id, initialDisplay, visibleDisplay) {
        initialDisplay = initialDisplay || 'initial';
        visibleDisplay = visibleDisplay  || 'initial';
        this._ids.add(id);
        this._initialDisplayById[id] = initialDisplay;
        this._visibleDisplayById[id] = visibleDisplay;

        return this;
    }

    /**
     * Makes a tab visible in the UI.
     * @param {string} id - The id of the element to show.
     * @return {TabManager}
     */
    showTab(id) {
        Array.from(this._ids.values()).forEach((entry) => {
            if (entry === id) {
                const element = Dom.getById(id);
                element.style.display = this._visibleDisplayById[entry];
            }
            else {
                const element = Dom.getById(entry);
                element.style.display = 'none';
                const f = this._onHideTabById[entry];
                if (f) f();
            }
        });

        const f = this._onShowTabById[id];
        if (f) f();

        return this;
    }

    /**
     * Sets all tabs to their default display modes.
     * @return {TabManager} this
     */
    init() {
        Array.from(this._ids.values()).forEach((id) => {
            Dom.getById(id).style.display = this._initialDisplayById[id];
        });

        return this;
    }

    /**
     * @public
     * @param {String} id
     * @param {Function} f
     * @return {TabManager} this
     */
    setOnShowTab(id, f) {
        this._onShowTabById[id] = f;

        return this
    }

    /**
     * @public
     * @param {String} id
     * @param {Function} f
     * @return {TabManager} this
     */
    setOnHideTab(id, f) {
        this._onHideTabById[id] = f;

        return this
    }
}
