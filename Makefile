DOCUMENTATION=documentation
DOCS_TMP=docs-tmp
ENV=environment
PYTHON=$(shell which python3)
PIP=$(PYTHON) -m pip
SLURP=./Slurp/slurp/build
SRC=./util,./math,./datastructures
DIST=dist
TEST_TEMP=./test-tmp
MOCHA=./node_modules/.bin/nyc ./node_modules/.bin/mocha
MOCHA_TEST_LOCATION='$(TEST_TEMP)/{,!(ui)/**}/test_*.js'
SUDO=$(shell which sudo)

create_env:
	virtualenv -p python3.6 env

essentials:
	$(SUDO) apt-get update
	$(SUDO) apt-get -y install curl git wget zlib1g-dev openssl

install: essentials
	git submodule update --init --recursive

	$(PIP) install --upgrade .

	$(SUDO) apt-get update
	curl -sL https://deb.nodesource.com/setup_16.x | $(SUDO) bash -
	$(SUDO) apt-get install -y nodejs

	npm install

init:
	git submodule update --init --recursive

test:
	@# Runs tests.
	@echo "${GREEN}Running tests${NC}"

	@rm -rf $(TEST_TEMP)
	@mkdir $(TEST_TEMP)

	@$(SLURP)/slurpUp.py $(SRC) $(TEST_TEMP) --fileExtensions=html,js,css --ignore=$(TEST_TEMP),node_modules \
	| $(SLURP)/fileLinker.py --fileExtensions=html,js,css \
	| $(SLURP)/spit.py

	@# Unit tests.
	@$(MOCHA) $(MOCHA_TEST_LOCATION) --recursive

	@# UI tests.
	# npx cypress run

	@rm -rf $(TEST_TEMP)

update_submodules:
	git submodule update --recursive

docs:
	@# Creates JS documentation.
	@npx documentation build --shallow --infer-private --sort-order="alpha" --format=html --output=$(DOCUMENTATION) ./aws ./backend ./components ./datastructures ./geography ./io ./graphics ./iterate ./math ./parse ./pwa ./storage ./util ./voodoo

	@rm -rf $(DOCS_TMP)
