import('../math/Random.js');

/**
 * @class
 */
class Color {

    /**
     * @public
     */
    static produceHex() {
        let result = ['#'];

        for (let i=0; i<6; i++) {
            result.push(Random.choice(this._HEX_VALID_CHARS));
        }

        return result.join('');
    }
}

Color._HEX_VALID_CHARS = '0123456789ABCDEF';