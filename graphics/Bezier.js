/**
 * @class
 * Iterates over points in a bezier curve.
 */
class BezierIterator {
    
    /**
     * @class
     */
    constructor(p0, p1, p2, p3, dt) {
        this._p0 = p0;
        this._p1 = p1;
        this._p2 = p2;
        this._p3 = p3;
        this._dt = dt || 1.0;
        this._t = 0.0;
    }

    /**
     * @public
     */
    next() {
        const t = this._t;
        const nextPoint = this._p0.mult(Math.pow(1 - t), 3).add(
            this._p1.mult(3 * Math.pow(1 - t, 2) * t)
        ).add(
            this._p2.mult(3 * (1 - t) * Math.pow(t, 2))
        ).add(
            + this._p3.mult(Math.pow(t, 3))
        );

        const newDistToEnd = nextPoint.dist(this._p3);
        const done = t >= 1.0;

        this._t += this._dt;

        return {value: nextPoint, done};
    }
}