/**
 * @class
 */
class JSString {
    
    /**
     * Returns the edit distance between two strings using the Wagner-Fischer algorithm.
     * @param {String} s
     * @param {String} t
     */
    static editDistance(s, t) {
        const m = s.length;
        const n = t.length;
        let d = [];

        for (let i = 0; i < m; i++) {
            let row = [];
            
            for (let j = 0; j < n; j++) {
                row.push(0);
            }

            d.push(row);
        }

        for (let i = 0; i < m; i++) {
            d[i][0] = i;
        }

        for (let j = 0; j < n; j++) {
            d[0][j] = j;
        }

        for (let j = 1; j < n; j++) {
            for (let i = 1; i < m; i++) {
                if (s[i] === t[j]) {
                    d[i][j] = d[i-1][j-1];
                }
                else {
                    d[i][j] = Math.min(d[i-1][j] + 1, d[i][j-1] + 1, d[i-1][j-1] + 1);
                }
            }
        }

        return d[m - 1][n - 1];
    }

}