/**
 * @class
 */
class Markdown {

    /**
     * @public
     */
    static toHTML(s) {
        let lines = s.split('\n');

        for (let i = 0; i < lines.length; i++) {
            let line = lines[i].trim();

            line = this._parseParagraphs(line);
            line = this._parseHeaders(line);
            line = this._parseLinks(line);

            lines[i] = line;
        }

        lines = lines.filter(line => line.length > 0);

        return lines.join('');
    }

    /**
     * @private
     */
    static _parseParagraphs(line) {
        if (line.startsWith('#') || line.startsWith('!')) return line;

        if (!line.length) return line;

        return `<p>${line}</p>`;
    }

    /** 
     * @private
     */
    static _parseHeaders(line) {
        let hCount = 0;

        while (hCount < line.length && line[hCount] === '#')
            hCount++;

        if (hCount > 0) {
            line = line.slice(hCount + 1).trim();
            line = `<h${hCount}>${line}</h${hCount}>`;
        }

        return line;
    }

    /** 
     * @private
     */
    static _parseLinks(line) {
        let openIndex, closeIndex, openLinkIndex, closeLinkIndex, all;
        let startIndex = 0;

        const findNextLink = () => {
            openIndex = line.indexOf('[', startIndex);
            closeIndex = line.indexOf(']', startIndex);
            openLinkIndex = line.indexOf('(', startIndex);
            closeLinkIndex = line.indexOf(')', startIndex);
            all = [openIndex, closeIndex, openLinkIndex, closeLinkIndex];
            startIndex = closeLinkIndex + 1;
        };

        findNextLink();

        while (startIndex != -1) {
            if (all.indexOf(-1) > -1) return line;
        
            if (openIndex < closeIndex < openLinkIndex < closeLinkIndex) {
                if (openIndex - 1 >= 0 && line[openIndex - 1] === '!') {
                    // Handle image.
                    const imageText = line.slice(openIndex + 1, closeIndex);
                    const imageHref = line.slice(openLinkIndex + 1, closeLinkIndex);

                    line = line.slice(0, openIndex - 1) + `<img src="${imageHref}" alt="${imageText}">`
                        + line.slice(closeLinkIndex + 1);
                } else {
                    // Handle link.
                    const linkText = line.slice(openIndex + 1, closeIndex);
                    const linkHref = line.slice(openLinkIndex + 1, closeLinkIndex);

                    line = line.slice(0, openIndex) + `<a href="${linkHref}">${linkText}</a>`
                        + line.slice(closeLinkIndex + 1);
                }
            } else {
                return line;
            }

            findNextLink();
        }
    }
}