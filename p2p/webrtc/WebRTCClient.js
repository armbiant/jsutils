
/**
 * Abstract base class for WebRTC connections.
 * Supports a single connection.
 * https://webrtc.org/getting-started/peer-connections#initiating_peer_connections
 * @class
 */
class WebRTCClient {

    /**
     * @constructor
     * @param {String} clientId
     * @param {String} sessionId
     */
    constructor(clientId, sessionId) {
        this._clientId = clientId;
        this._sessionId = sessionId;
        this._remoteClientId = null;
        this._isHosting = false;

        this._peerConnection = null;
        this._dataChannel = null;

        this._rtcOffer = null;
        this._signallingPollInterval = null;
        this._expectMessageType = 'answer';

        this._onConnectCallbacks = [];
        this._onMessageCallbacks = [];

        this._init();
    }

    /**
     * @public
     */
    get id() {
        return this._clientId;
    }

    /**
     * @public
     */
    get remoteId() {
        return this._remoteClientId;
    }

    /**
     * Send a message to the signalling channel.
     * @private
     * @abstract
     * @param {Object} message
     * @return {Promise}
     */
    _sendToSignallingChannel(message) {

    }

    /**
     * Fetch messages from the signalling channel.
     * @private
     * @abstract
     * @return {Promise<messages[]>}
     */
    _pollSignallingChannel() {

    }

    /**
     * @public
     */
    onConnect(callback) {
        this._onConnectCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    onMessage(callback) {
        this._onMessageCallbacks.push(callback);

        return this;
    }

    /**
     * @public
     */
    becomeHost() {
        this._isHosting = true;
        this._expectMessageType = 'offer';

        return this;
    }

    /**
     * @private
     * @return {Promise}
     */
    _init() {
        const configuration = {
            'iceServers': [{'urls': 'stun:stun.l.google.com:19302'}]
        };

        this._peerConnection = new RTCPeerConnection(configuration);

        // Listen for local ICE candidates on the local RTCPeerConnection
        this._peerConnection.addEventListener('icecandidate', event => {
            if (event.candidate) {
                this._sendToSignallingChannel({
                    'new-ice-candidate': event.candidate,
                    remoteClientId: this._remoteClientId,
                });
            }
        });

        // Listen for connectionstatechange on the local RTCPeerConnection
        this._peerConnection.addEventListener('connectionstatechange', event => {
            const state = this._peerConnection.connectionState;

            if (this._peerConnection.connectionState === 'connected') {
                // Peers connected!
                console.log('Connected!');
                this._onConnectCallbacks.forEach(c => c());
                this._stopSignallingPolling();
            } else if (state === 'failed') {
                // Uh oh speghetti.
                this.tearDown();
                this._init();
            }
        });

        // Begin polling signalling channel.
        this._startSignallingPolling();

        this._initDataChannel();

        return this._peerConnection.createOffer().then((offer) => {
            this._rtcOffer = offer;

            return this._peerConnection.setLocalDescription(offer).then(() => {
                return this._sendToSignallingChannel({'offer': offer});
            });
        });
    }

    /**
     * @private
     */
    _initDataChannel() {
        const localChannel = this._peerConnection.createDataChannel('default-channel');

        this._peerConnection.addEventListener('datachannel', event => {
            this._dataChannel = event.channel;
        });

        localChannel.addEventListener('message', (event) => {
            this._onMessageCallbacks.forEach(c => c(JSON.parse(event.data)));
        });
    }

    /**
     * @public
     */
    isConnected() {
        return this._peerConnection && this._peerConnection.connectionState === 'connected';
    }

    /**
     * @public
     */
    isHost() {
        return Boolean(this._remoteClientId);
    }

    /**
     * Close all connections, stop all polling.
     * @public
     */
    tearDown() {
        this._stopSignallingPolling();
    }

    /**
     * @public
     * @return {Promise}
     */
    sendMessage(message) {
        // TODO.
        // Ensure data channel is open.
        this._dataChannel.send(
            JSON.stringify(message)
        );
    }

    /**
     * @private
     */
    _startSignallingPolling() {
        this._signallingPollInterval = setInterval(() => {
            this._pollSignallingChannel().then(messages => {
                messages.forEach(message => {
                    if (message) {
                        this._handleSignallingMessage(message);
                    }
                });
            });
        }, this.constructor._POLL_INTERVAL_MS);
    }

    /**
     * @private
     */
    _stopSignallingPolling() {
        if (this._signallingPollInterval) {
            clearInterval(this._signallingPollInterval);

            this._signallingPollInterval = null;
        }
    }

	/**
     * @private
     * @return {Promise}
     */
    _handleSignallingMessage(message) {
        if (message.remoteClientId) {
            this._remoteClientId = message.remoteClientId;
        }

        if (!message[this._expectMessageType]) {
            return;
        }

        // Calling side.
        if (message.answer) {
            this._expectMessageType = 'iceCandidate';
            
            const remoteDesc = new RTCSessionDescription(message.answer);
            
            return this._peerConnection.setRemoteDescription(remoteDesc);
        }

        // Receiving side.
        if (message.offer) {
            this._expectMessageType = 'iceCandidate';

            this._peerConnection.setRemoteDescription(new RTCSessionDescription(message.offer));

            return this._peerConnection.createAnswer().then((answer) => {
                this._sendToSignallingChannel({
                    answer: answer,
                    remoteClientId: this._remoteClientId,
                });

                return this._peerConnection.setLocalDescription(answer);
            });
        }

        // Both sides.
        if (message.iceCandidate) {
            message.iceCandidate.forEach((candidate) => {
                try {
                    return this._peerConnection.addIceCandidate(candidate);
                } catch (e) {
                    console.error('Error adding received ice candidate', e);
                }
            });
        }
    }
}

WebRTCClient._POLL_INTERVAL_MS = 1000;
