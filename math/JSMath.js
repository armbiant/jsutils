
/**
 * class
 */
class JSMath {

    /**
     * @public
     */
    static _maxima(items, key, comparator) {
        let min = null;
        let minValue = null;

        for (let item of items) {
            let value = item;

            if (key) value = key(item);

            if (min === null || comparator(value, minValue)) {
                min = item;
                minValue = value;
            }
        }

        return min;
    }

    /**
     * @public
     * @param {Array} items
     * @param {?Function} key - returns Number
     */
    static min(items, key) {
        return this._maxima(items, key, (a, b) => a < b);
    }

    /**
     * @public
     * @param {Array} items
     * @param {?Function} key - returns Number
     */
    static max(items, key) {
        return this._maxima(items, key, (a, b) => a > b);
    }
}
