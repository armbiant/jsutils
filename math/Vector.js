import('Matrix.js');

/**
 * @class
 */
class Vector {

    /**
     * @constructor
     */
    constructor(vector) {
        if (arguments.length > 1) {
            this._vector = Array.from(arguments).slice();
        } else {
            this._vector = (vector || []).slice();
        }
    }

    /**
     * @public
     */
    get x() {
        return this._vector[0];
    }

    /**
     * @public
     */
    set x(value) {
        this._vector[0] = value;
    }

    /**
     * @public
     */
    get y() {
        return this._vector[1];
    }

    /**
     * @public
     */
    set y(value) {
        this._vector[1] = value;
    }

    /**
     * @public
     */
    get z() {
        return this._vector[2] || 0;
    }

    /**
     * @public
     */
    set z(value) {
        this._vector[2] = value;
    }

    /**
     * @private
     */
    _performComponentwiseOperation(vector, operation) {
        let result = [];

        for (let i=0; i < vector._vector.length; i++) {
            const a = this._vector[i] || 0;
            const b = vector._vector[i];

            result.push(operation(a, b));
        }

        return new Vector(result);
    }

    /**
     * @public
     */
    copy() {
        return new Vector(this._vector);
    }

    /**
     * @public
     */
    zero() {
        let newVector = [];

        for (let i=0; i < this._vector.length; i++) {
            newVector.push(0.0);
        }

        let result = this.copy();
        result.setValues(newVector);

        return result;
    }

    /**
     * @public
     * @return {Number}
     */
    sum() {
        return this._vector.reduce((a, v) => a + v, 0);
    }

    /**
     * @public
     * @return {Number}
     */
    magnitude() {
        const sum = this._vector.reduce((a, v) => a + Math.pow(v, 2), 0);

        return Math.sqrt(sum);
    }

    /**
     * @public
     * @return{Vector}
     */
    normalize() {
        const total = this.magnitude();

        if (total == 0) {
            throw new Error('Vector magnitude is zero.');
        }

        const values = this._vector.map(v => v / total);

        return new Vector(values);
    }

    /**
     * @public
     */
    perpendicular() {
        return new Vector(this.y, -this.x);
    }

    /**
     * @public
     */
    projectOnto(v) {
        const numerator = this.dotProduct(v);
        const denomenator = Math.pow(v.magnitude(), 2);

        return v.multiply(numerator / denomenator);
    }

    /**
     * Returns the angle of a 2d vector.
     * @public
     */
    angle() {
        return Math.atan2(this._vector[1], this._vector[0]);
    }

    /**
     * Returns the vector as a list of numbers.
     * @public
     * @return {Number[]}
     */
    getValues() {
        return this._vector;
    }

    /**
     * Sets the vector data.
     * @public
     * @param {Number[]} values
     */
    setValues(values) {
        this._vector = values;
    }

    /**
     * @private
     */
    _performScalarOperation(scalar, operation) {
        let result = this.copy();

        result._vector = result._vector.map(v => {
            return operation(v, scalar);
        });

        return result;
    }

    /**
     * @public
     * @param {Number|Vector} value
     * @return {Vector}
     */
    add(value) {
        const operation = (a, b) => a + b;

        if (!(value instanceof Vector)) {
            return this._performScalarOperation(value, operation)
        }

        return this._performComponentwiseOperation(value, operation);
    }

    /**
     * @public
     * @param {Number|Vector} value
     * @return {Vector}
     */
    subtract(value) {
        const operation = (a, b) => a - b;

        if (!(value instanceof Vector)) {
            return this._performScalarOperation(value, operation)
        }

        return this._performComponentwiseOperation(value, operation);
    }

    /**
     * @public
     * @param {Number} value
     * @return {Vector}
     */
    multiply(value) {
        const result = this._vector.map(v => v * value);

        return new Vector(result);
    }

    /**
     * @public
     * TODO: N dimensions.
     */
    crossProduct(v2) {
        const x = this.y * v2.z - this.z * v2.y;
        const y = this.z * v2.x - this.x * v2.z;
        const z = this.x * v2.y - this.y * v2.x;

        return new Vector(x, y, z);
    }

    /**
     * @public
     */
    dotProduct(v2) {
        let total = 0;

        for (let i in this._vector) {
            total += this._vector[i] * v2._vector[i];
        }

        return total;
    }

    /**
     * @public
     */
    angularDifference(v2) {
        return Math.acos(this.dotProduct(v2)
            / (this.magnitude() * v2.magnitude()));
    }

    /**
     * @public
     */
    applyTorque(torqueV) {
        const clockwise = this.perpendicular();
        const polarDelta = torqueV.projectOnto(clockwise);
        const sign = -1 * Math.sign(polarDelta.dotProduct(clockwise));
        
        return sign * polarDelta.magnitude();
    }

    /**
     * @public
     * @param {Number} value
     * @return {Vector}
     */
    divide(value) {
        const result = this._vector.map(v => v / value);

        return new Vector(result);
    }

    /**
     * @private
     */
    _getPointMatrix() {
        return new Matrix(
            [this.x],
            [this.y],
            [this.z],
        );
    }

    /**
     * @public
     * @param {Number} x
     * @param {Number} y
     * @param {Number} z
     * @return {Vector}
     */
    translate(x, y, z) {
        z = z || 0;

        return new Vector(this.x + x, this.y + y, this.z + z);
    }

    /**
     * Rotates a vector representing a 3d point about the origin.
     * @public
     * @param {Number} theta
     * @return {Vector}
     */
    rotateZ(theta) {
        const rotationMatrix = new Matrix(
            [Math.cos(theta), -Math.sin(theta), 0],
            [Math.sin(theta), Math.cos(theta), 0],
            [0, 0, 1],
        );

        let result = rotationMatrix.multiply(this._getPointMatrix());
        result = result.getColumn(0);

        return new Vector(result);
    }
}