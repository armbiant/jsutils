import('../Vector.js');
import('../../util/assert.js');

suite('Vector.js', () => {
    suite('zero()', () => {
        spec('Should have no side effects', () => {
            let v = new Vector([1, 2, 3]);

            v.zero();

            const values = v.getValues();

            values.forEach(value => {
                assertNotEqual(value, 0);
            })
        });
    });
});