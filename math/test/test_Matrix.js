import('../Matrix.js');
import('../../util/assert.js');

suite('Matrix.js', () => {
    suite('multiply()', () => {
        spec('Should have identity property', () => {
            const identity = new Matrix([
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1],
            ]);

            const jumble = new Matrix([
                [9, 4, 3],
                [8, 5, 2],
                [7, 6, 1],
            ]);

            const result = identity.multiply(jumble);

            assertDeepEqual(result._data, jumble._data)
        });
    });

    spec('transpose', () => {
        const result = new Matrix([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ]).transpose();
        const expected = new Matrix([
            [1, 4, 7],
            [2, 5, 8],
            [3, 6, 9],
        ]);

        assertDeepEqual(result._data, expected._data);
    });

});