import('LinearInterpolation.js');
import('Random.js');

/**
 * @class
 */
class Perlin {

    /**
     * @private
     */
    static _computeGradient(ix, iy) {
        if (!Array.isArray(this._GRADIENT[iy])) {
            this._GRADIENT[iy] = [];
        }

        if (!Array.isArray(this._GRADIENT[iy][ix])) {
            this._GRADIENT[iy][ix] = [];
        }

        if (!this._GRADIENT[iy][ix].length) {
            const theta = Random.range(0, 2 * Math.PI);
            const x = Math.cos(theta);
            const y = Math.sin(theta);

            this._GRADIENT[iy][ix] = [x, y];
        }
    }

    /**
     * @private
     */
    static _dotGridGradient(ix, iy, x, y) {
        ix = Math.floor(ix);
        iy = Math.floor(iy);

        this._computeGradient(ix, iy);

        let dx = x - ix;
        let dy = y - iy;

        let gx = this._GRADIENT[iy][ix][0];
        let gy = this._GRADIENT[iy][ix][1];

        return dx * gx + dy * gy;
    }

    /**
     * @public
     * https://en.wikipedia.org/wiki/Perlin_noise#Algorithm_detail
     */
    static get(x, y, scale, xOffset, yOffset) {
        if (xOffset === undefined) xOffset = 0.1;
        if (yOffset === undefined) yOffset = 0.1;
        scale = scale || 1.0;

        x = x * scale + xOffset;
        y = y * scale + yOffset;

        let x0 = Math.floor(x);
        let x1 = x0 + 1;
        let y0 = Math.floor(y);
        let y1 = y0 + 1;

        let sx = x - x0;
        let sy = y - y0;

        let n0, n1, ix0, ix1;

        n0 = this._dotGridGradient(x0, y0, x, y);
        n1 = this._dotGridGradient(x1, y0, x, y);

        // ix0 = LinearInterpolation.compute(n0, n1, sx);
        ix0 = (n1 - n0) * (3 - sx * 2) * sx * sx + n0;

        n0 = this._dotGridGradient(x0, y1, x, y);
        n1 = this._dotGridGradient(x1, y1, x, y);
        // ix1 = LinearInterpolation.compute(n0, n1, sx);
        ix1 = (n1 - n0) * (3 - sx * 2) * sx * sx + n0;

        // return LinearInterpolation.compute(ix0, ix1, sy);
        return (ix1 - ix0) * (3 - sy * 2) * sy * sy + ix0;
    }

    /**
     * @public
     */
    static reset() {
        Perlin._GRADIENT = [];

        return this;
    }
}

Perlin._GRADIENT = [];
